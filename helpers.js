function rgbaToStr(rgba){
    // convert rgba(float, float, float, float) to an actual list
    rgba = rgba.join(", ");
    rgba = "rgba("+rgba+")";
    return rgba;
}

function showContacts(){
    contactsBox = document.getElementById('contacts-box');
    contactsBox.scrollIntoView(true);

    contactsBox.style.backgroundColor = "rgba(0,0,0, .33)"
    setTimeout(() => {
        contactsBox.style.backgroundColor = "rgba(0,0,0,.09)"
    }, 680);
}